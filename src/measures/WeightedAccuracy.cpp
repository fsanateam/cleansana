/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WeightedAccuracy.cpp
 * Author: user
 * 
 * Created on November 7, 2018, 7:22 PM
 */

#include "WeightedAccuracy.hpp"

WeightedAccuracy::WeightedAccuracy(Graph* G1, Graph* G2) : Measure(G1, G2, "weighted_acc") {
}

WeightedAccuracy::~WeightedAccuracy() {
}

double WeightedAccuracy::eval(const Alignment& A){
    double beta = B;
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    double weighted_acc = (beta*Ea + omega - (E1 + Ea_hat - Ea)) / (beta * E1 + omega - E1);
    return weighted_acc;
}


