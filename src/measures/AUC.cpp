/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AUC.cpp
 * Author: user
 * 
 * Created on December 26, 2018, 9:21 PM
 */

#include "AUC.hpp"


AUC::AUC(Graph* G1, Graph* G2) : Measure(G1, G2, "auc") {
}

AUC::~AUC() {
}

double AUC::eval(const Alignment& A){
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    double auc = 0.5 * (1 + (Ea / Ea_hat) - ((E1 - Ea) / (omega - Ea_hat)));
    return auc;
}

