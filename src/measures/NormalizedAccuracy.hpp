/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NormalizedAccuracy.hpp
 * Author: user
 *
 * Created on October 2, 2018, 3:55 PM
 */

#ifndef NORMALIZEDACCURACY_HPP
#define NORMALIZEDACCURACY_HPP
#include "Accuracy.hpp"

class NormalizedAccuracy: public Measure{
public:
    NormalizedAccuracy(Graph* G1, Graph* G2);
    virtual ~NormalizedAccuracy();
    double eval(const Alignment& A);
    double getMinAccuracy();
private:
    double minAccuracy = 0;
};

#endif /* NORMALIZEDACCURACY_HPP */

