/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ROC.cpp
 * Author: user
 * 
 * Created on December 26, 2018, 9:26 PM
 */

#include "ROC.hpp"

using namespace std;

ROC::ROC(Graph* G1, Graph* G2): Measure(G1, G2, "roc") {
}

ROC::~ROC() {
}

double ROC::eval(const Alignment& A){
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    double fpr = (E1 - Ea) / (omega - Ea_hat);
    double tpr_minus = 1 - (Ea / Ea_hat);
    double f_beta = fpr * fpr + tpr_minus * tpr_minus;
    return f_beta;
}

