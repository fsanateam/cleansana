/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NormalizedAccuracy.cpp
 * Author: user
 * 
 * Created on October 2, 2018, 3:55 PM
 */

#include "NormalizedAccuracy.hpp"

NormalizedAccuracy::NormalizedAccuracy(Graph* G1, Graph* G2) : Measure(G1, G2, "nor_acc") {
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    minAccuracy = 1 - (G1->getNumEdges() + G2->getNumEdges()) / omega;
}

double NormalizedAccuracy::getMinAccuracy(){
    return minAccuracy;
}

NormalizedAccuracy::~NormalizedAccuracy() {
}

double NormalizedAccuracy::eval(const Alignment& A){
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    double acc = 1 - (E1 + Ea_hat - 2 * Ea) / omega;
    return (acc - minAccuracy) / (1 - minAccuracy);
}

