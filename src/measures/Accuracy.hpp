/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Accuracy.hpp
 * Author: user
 *
 * Created on October 2, 2018, 1:43 PM
 */

#ifndef ACCURACY_HPP
#define ACCURACY_HPP
#include "Measure.hpp"

class Accuracy: public Measure {
public:
    Accuracy(Graph* G1, Graph* G2);    
    virtual ~Accuracy();
    double eval(const Alignment& A);
private:

};

#endif /* ACCURACY_HPP */

