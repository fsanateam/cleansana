/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NewAccuracy.cpp
 * Author: user
 * 
 * Created on December 27, 2018, 4:49 PM
 */

#include "NewAccuracy.hpp"

NewAccuracy::NewAccuracy(Graph* G1, Graph* G2): Measure(G1, G2, "new_acc") {
}

NewAccuracy::~NewAccuracy() {
}

double NewAccuracy::eval(const Alignment& A){
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    double C = (omega - E1) / E1;
    double new_acc = ((Ea / Ea_hat) + C * (1 - ((E1 - Ea) / (omega - Ea_hat)))) / (1 + C);
    return new_acc;
}