/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fbeta.cpp
 * Author: user
 * 
 * Created on December 10, 2018, 11:42 AM
 */

#include "Fbeta.hpp"

Fbeta::Fbeta(Graph* G1, Graph* G2): Measure(G1, G2, "f_beta") {
}

Fbeta::~Fbeta() {
}

double Fbeta::eval(const Alignment& A){
    double beta = B;
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double f_beta = ((1 + beta) * Ea) / (E1 + beta * Ea_hat);
    return f_beta;
}
