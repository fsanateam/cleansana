/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fbeta.hpp
 * Author: user
 *
 * Created on December 10, 2018, 11:42 AM
 */

#ifndef FBETA_HPP
#define FBETA_HPP
#include "Measure.hpp"

class Fbeta : public Measure{
public:
    Fbeta(Graph* G1, Graph* G2);
    virtual ~Fbeta();
    double eval(const Alignment& A);
    double B = 1.0;
    void setBeta(double beta){B=beta;};
    double getBeta(){return B;};
private:

};

#endif /* FBETA_HPP */

