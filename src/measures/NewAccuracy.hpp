/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NewAccuracy.hpp
 * Author: user
 *
 * Created on December 27, 2018, 4:49 PM
 */

#ifndef NEWACCURACY_HPP
#define NEWACCURACY_HPP
#include "Measure.hpp"

class NewAccuracy:public Measure {
public:
    NewAccuracy(Graph* G1, Graph* G2);   
    virtual ~NewAccuracy();
    double eval(const Alignment& A);
private:

};

#endif /* NEWACCURACY_HPP */

