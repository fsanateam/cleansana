/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WeightedAccuracy.hpp
 * Author: user
 *
 * Created on November 7, 2018, 7:22 PM
 */

#ifndef WEIGHTEDACCURACY_HPP
#define WEIGHTEDACCURACY_HPP
#include "Measure.hpp"

class WeightedAccuracy: public Measure {
public:
    WeightedAccuracy(Graph* G1, Graph* G2);    
    virtual ~WeightedAccuracy();
    double eval(const Alignment& A);
    double B = 1.0;
    void setBeta(double beta){B=beta;};
    double getBeta(){return B;};
private:

};

#endif /* WEIGHTEDACCURACY_HPP */

