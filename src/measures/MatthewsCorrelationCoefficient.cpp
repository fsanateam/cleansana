/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MatthewsCorrelationCoefficient.cpp
 * Author: user
 * 
 * Created on November 19, 2018, 10:34 AM
 */

#include "MatthewsCorrelationCoefficient.hpp"

MatthewsCorrelationCoefficient::MatthewsCorrelationCoefficient(Graph* G1, Graph* G2): Measure(G1, G2, "mcc") {
}

MatthewsCorrelationCoefficient::~MatthewsCorrelationCoefficient() {
}

double MatthewsCorrelationCoefficient::eval(const Alignment& A){
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    
    double numerator = omega * Ea - E1 * Ea_hat;
    double denominator = sqrt(E1 * Ea_hat * (omega - E1) * (omega - Ea_hat));
    return numerator / denominator;
}

