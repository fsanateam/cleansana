/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ROC.hpp
 * Author: user
 *
 * Created on December 26, 2018, 9:26 PM
 */

#ifndef ROC_HPP
#define ROC_HPP
#include "Measure.hpp"

class ROC : public Measure{
public:
    ROC(Graph* G1, Graph* G2);
    virtual ~ROC();    
    double eval(const Alignment& A);
private:

};

#endif /* ROC_HPP */

