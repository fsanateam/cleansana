/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AUC.hpp
 * Author: user
 *
 * Created on December 26, 2018, 9:21 PM
 */

#ifndef AUC_HPP
#define AUC_HPP
#include "Measure.hpp"

class AUC : public Measure{
public:
    AUC(Graph* G1, Graph* G2);
    virtual ~AUC();
    double eval(const Alignment& A);
private:

};

#endif /* AUC_HPP */

