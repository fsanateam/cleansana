/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   F1Score.hpp
 * Author: user
 *
 * Created on November 28, 2018, 12:33 AM
 */

#ifndef F1SCORE_HPP
#define F1SCORE_HPP
#include "Measure.hpp"

class F1Score : public Measure{
public:
    F1Score(Graph* G1, Graph* G2);
    virtual ~F1Score();
    double eval(const Alignment& A);
private:

};

#endif /* F1SCORE_HPP */

