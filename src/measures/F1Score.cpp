/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   F1Score.cpp
 * Author: user
 * 
 * Created on November 28, 2018, 12:33 AM
 */

#include "F1Score.hpp"

F1Score::F1Score(Graph* G1, Graph* G2) : Measure(G1, G2, "f1") {
}

double F1Score::eval(const Alignment& A){
    double beta = G2->getNumEdges() / ((double) G1->getNumEdges());
    double E1 = G1->getNumEdges();
    double Ea_hat = G2->numNodeInducedSubgraphEdges(A.getMapping());
    double Ea = A.numAlignedEdges(*G1, *G2);
    double omega = G1->getNumNodes() * (G1->getNumNodes() - 1) / 2;
    return (2 * Ea) / (E1 + Ea_hat);
}

F1Score::~F1Score() {
}

